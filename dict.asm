global find_word


%include "lib.inc"

section .text

find_word:
    cmp rsi, 0
    je .undetected
    add rsi, 8
    mov r10,rdi
    mov r11,rsi
    call string_equals
    mov rdi,r10
    mov rsi,r11
    test rax, rax
    jnz  .pointer
    mov rsi, [rsi - 8]
    jmp find_word

.pointer:
    sub rsi, 8
    mov rax, rsi
    ret

.undetected:
    xor rax, rax
    ret
