%macro colon 2

%ifid %2
    %ifdef LASTLABEL
        %2: dq LASTLABEL
    %else
        %2: dq 0
    %endif

    %define LASTLABEL %2
%else
    %fatal "Number or string can not be label"
%endif 

%ifstr %1
    %%key_string:   db   %1, 0
%else
    %error "Identifiers or Number can not be key"
%endif

%endmacro

