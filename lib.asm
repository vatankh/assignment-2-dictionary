global read_word, parse_uint, parse_int, string_equals, string_copy, exit, string_length, print_string, print_char, print_newline, print_uint, print_int, read_char

section .data
temp: db 2

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60   
    mov rdi, 0    
    syscall 
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax          
.theloop:      
    cmp byte[rdi+rax], 0  ;check if value is zero 
    je .return               ;end loop if value is zero 
    inc rax               ;increas rax by one
    jmp .theloop          ;loop
.return:
    ret   

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length    ;in rax we have the length of our string 
    mov rdx, rax          ;string length in bytes put in rax
    mov rax, 1            ;syscall number (1 - write)
    mov rsi, rdi          ;read string in adress rdi 
    mov rdi, 1            ;write in stodout 
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov [temp], di   ; put char in var temp
    mov rdi,temp     ; put addres var in temp
    call print_string     ;print char
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10           ;new line symbol in ascii
    call print_char       ;print new line
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi          ;clea ac
    push rbx              ;save rbx
    mov rbx, 10           ;put rbx=10 because число в десятичном формате 
    mov rdi, rsp          ;put address stack in rdi
    push 0                ;zero in order to know where to finish 
    dec rdi               ;decressing rdi so it have address the value 0 in stack
    sub rsp, 13           ;give 13 places to put te values of the number 
.theloop:
    xor rdx, rdx          ;rdx=0
    div rbx               ;divide by  10, remaning rdx, result в rax
    add rdx, 0x30         ;add 0x30, in ordeer to get  ASCII code 
    dec rdi               ;rdi --
    mov [rdi], dl         ;put the number in stack 
    test rax, rax         ;if 0 
    jne .theloop             ;if 0 exit loop
    call print_string     
    add rsp, 21           
    pop rbx               
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0            
    jge .positive                             
    push rdi              
    mov rdi, 0x2D         ;adding symbol '-'
    call print_char       
    pop rdi              
    neg rdi             
.positive:
    call print_uint       
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length     
    mov rcx, rax          
    xchg rdi, rsi          
    call string_length     
    cmp rax, rcx           
    jne .noequal             
.theloop:
    dec rcx                
    mov rax, [rdi+rcx]      
    cmp al, [rsi+rcx]      
    jne .noequal             
    cmp rcx, 0             
    jg .theloop               
    mov rax, 1              
    ret                 
.noequal:
    xor rax, rax           
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax           
    xor rdi, rdi           
    push 0                 
    mov rsi, rsp           
    mov rdx, 1             
    syscall                 
    pop rax               
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        xor rcx, rcx            
.theloop:
    push rdi                
    push rsi                
    push rcx                
    call read_char         
    pop rcx                 
    pop rsi
    pop rdi
    cmp rax, 0xA            
    je .isinstart            
    cmp rax, 0x9           
    je .isinstart           
    cmp rax, 0x20            
    je .isinstart           
    cmp rax, 0x0           
    je .return                 
    mov [rdi+rcx], rax      
    inc rcx                 
    cmp rcx, rsi            
    jl .theloop                
    xor rax, rax            
    xor rdx, rdx           
    ret                 
.isinstart:
    test rcx, rcx           
    jz .theloop               
.return:
    mov byte[rdi+rcx], 0    
    mov rax, rdi          
    mov rdx, rcx            
    ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r12  
    push rbx                         
                
    mov rbx, 10       
    xor rax, rax  
    xor rcx, rcx            

.theloop:
    mov r12, [rdi+rcx]      
    and r12, 0xff           
    cmp r12b, 0             
    je .return                 
    cmp r12b, 0x30          
    jb .return                 
    cmp r12b, 0x39            
    ja .return                 
    mul rbx                 
    sub r12b, 0x30         
    add rax, r12          
    inc rcx                 
    jmp .theloop
.return:
    mov rdx, rcx            
    pop rbx
    pop r12                 
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rbx                
    mov rbx, [rdi]          
    and rbx, 0xff           
    cmp bl, 0x2D            
    je .parsenegative                 
    call parse_uint         
    jmp .return                 
.parsenegative:  
    inc rdi                 
    call parse_uint         
    test rdx, rdx          
    jz .return                
    neg rax                 
    inc rdx                 
.return:
    pop rbx                 
    ret 
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length      
    inc rax                 
    cmp rax, rdx            
    jg .error                
    xor rcx, rcx            
.theloop:
    mov rdx, [rdi+rcx]      
    mov [rsi+rcx], rdx      
    inc rcx                 
    cmp byte[rdi+rcx], 0    
    jne .theloop               
    ret
.error:
    xor rax, rax            
    ret

