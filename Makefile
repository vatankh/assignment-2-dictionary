ASM=nasm
ASMFLAGS=-f elf64 
LD=ld

all: main
	
main: main.o dict.o lib.o 
	$(LD) -o main main.o dict.o lib.o

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

clean:
	$(RM) -rf *.o main
