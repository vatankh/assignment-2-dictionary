%include "colon.inc"
%include "words.inc"
%include "lib.inc"

global _start



%define STDOUT 0x01
%define STDERR 0x02

section .rodata
type_key_message: db "type key: ", 0
no_such_key: db "ERROR: no such key in dict", 10, 0
input_is_long: db "ERROR: input  is very long !", 10, 0

section .text
_start:
    sub rsp, 256
    mov rdi, type_key_message
    mov rsi, STDOUT
    call print_string
    mov rdi, rsp
    mov rsi, 256
    call read_word
    cmp rax, 0
    mov r9 ,input_is_long
    je .out_error
    mov rdi, rax
    mov rsi, LASTLABEL 
    call find_word
    test rax, rax
    mov  r9 ,no_such_key
    jz .out_error
.out_key_value:
    mov rdi, rax 
    add rdi, 8      ; address of key element
     mov r10, rdi
    call string_length
    mov rdi,r10
    add rdi, rax  
    inc rdi     ; goto value (key length + 1)
    mov rsi, STDOUT
    call print_string
    call print_newline
    add rsp, 256
    jmp exit

.out_error:
    mov rdi, r9
    mov rsi, STDERR
    call print_string
    add rsp, 256
    jmp exit
